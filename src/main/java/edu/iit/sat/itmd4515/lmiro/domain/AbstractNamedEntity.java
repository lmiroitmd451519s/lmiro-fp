/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.domain;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotBlank;

/**
 *
 * @author lynnmiro
 */
@MappedSuperclass
public abstract class AbstractNamedEntity extends AbstractIdentifiedEntity {

    /**
     * name: property from data type String
     */
    @NotBlank(message = "Name is required")
    @Column(nullable = false, unique = true)
    protected String name;

    /**
     * AbstractNamedEntity Class constructor
     */
    public AbstractNamedEntity() {
    }

    /**
     *
     * @param name
     */
    public AbstractNamedEntity(String name) {
        this.name = name;
    }

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

}
