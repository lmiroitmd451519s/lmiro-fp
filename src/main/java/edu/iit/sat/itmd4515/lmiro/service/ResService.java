/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.service;

import edu.iit.sat.itmd4515.lmiro.domain.Customer;
import edu.iit.sat.itmd4515.lmiro.domain.Reservation;
import edu.iit.sat.itmd4515.lmiro.web.ResturantController;
import static java.lang.Math.log;
import static java.lang.StrictMath.log;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Named;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author lynnmiro
 */
@Named
@Stateless
public class ResService extends AbstractService<Reservation> {

    /**
     * this ResService Class Constructor
     */
    public ResService() {
        super(Reservation.class);
    }

    /**
     * this method return the list of reservations in the db as
     * List<Reservation>
     *
     * @return the list of reservations
     */
    
    @Override
    public List<Reservation> findAll() {
        return em.createNamedQuery("Reservation.findAll", entityClass).getResultList();
    }

    /**
     * this method bring the front-end customer into persistence context and
     * create the new reservation from user input making sure the new
     * reservation has its db generated id and setting both sides of the
     * relationship. It also merging the customer, because this entity is the
     * owning side of the relationship it controls the database updates and
     * creates the FK
     * @param v Customer
     * @param c Reservation
     */
    public void createWithCustomer(Reservation v, Customer c) {
        c = em.getReference(Customer.class, c.getId());
        super.create(v);
        em.flush();
        c.addReservation(v);
        em.merge(c);
    }

    /**
     * This service method accept a Reservation POJO representing the input from
     * the customer. This method will only update the date, time, and party number of
     * the Reservation. It will not change the customer.
     * @param resFromCustomerForm
     */
    public void editResNoChangeToCustomer(Reservation resFromCustomerForm) {
        Reservation resFromCDatabase = em.getReference(Reservation.class, resFromCustomerForm.getId());

        resFromCDatabase.setDate(resFromCustomerForm.getDate());
        resFromCDatabase.setTime(resFromCustomerForm.getTime());
        resFromCDatabase.setPartyNum(resFromCustomerForm.getPartyNum());

        em.merge(resFromCDatabase);
    }
//    @Override
//    public void remove (Reservation resFromCustomerForm){
//        Reservation resFromCDatabase = em.getReference(Reservation.class, resFromCustomerForm.getId());
//        List<Reservation> resToRemove = new ArrayList<>(resFromCustomerForm.getCustomer().getReservations());
//        
//        
//        em.remove(resToRemove);
//      
//        
//        Customer c = resFromCustomerForm.getCustomer();
//        c.removeReservation(resFromCDatabase);
//        em.merge(c);
//      
//
//        em.remove(resFromCDatabase);        
//    }


}
