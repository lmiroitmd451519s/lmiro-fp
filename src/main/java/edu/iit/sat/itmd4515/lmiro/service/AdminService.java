/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.service;

import edu.iit.sat.itmd4515.lmiro.domain.AdminRest;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * @author lynnmiro
 */
@Named
@Stateless
public class AdminService extends AbstractService<AdminRest>{

    /**
     *this AdminService Class Constructor
     */
    public AdminService() {
        super(AdminRest.class);
    }
    /**
     * this method return the list of AdminRests in the db as
     * List<AdminRest>
     *
     * @return the list of AdminRests
     */
    @Override
    public List<AdminRest> findAll() {
        return em.createNamedQuery("AdminRest.findAll", entityClass).getResultList();
    }
    
    /**
     *this method finds the AdminRest by its username that defines in its POJO class
     * @param username
     * @return the AdminRest found
     */
    public AdminRest findByUsername(String username){
        return em.createNamedQuery("AdminRest.findByUsername", AdminRest.class).setParameter("username", username).getSingleResult();
    }
}
