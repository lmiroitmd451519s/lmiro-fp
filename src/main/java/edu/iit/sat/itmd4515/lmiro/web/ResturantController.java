/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// mini controllers// backing beans JSF
package edu.iit.sat.itmd4515.lmiro.web;

import edu.iit.sat.itmd4515.lmiro.domain.RestType;
import edu.iit.sat.itmd4515.lmiro.domain.Resturant;
import edu.iit.sat.itmd4515.lmiro.service.ResturantService;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author lynnmiro
 */
@Named
@RequestScoped
public class ResturantController {

    private static final Logger LOG = Logger.getLogger(ResturantController.class.getName());

    @EJB
    private ResturantService resturantSvc;

    @Inject
    LoginController loginController;

    private Resturant resturant;

    /**
     * this constructor of ResturantController class
     */
    public ResturantController() {
    }

    @PostConstruct
    private void postConstruct() {
        LOG.info("inside restController.postConstruct()");

        resturant = new Resturant();
    }

    /**
     * this method to fire action in the front end when clicking on View
     * Restaurant
     *
     * @param resturant
     * @return redirect the user to the view Restaurant page of the admin pages
     */
    public String prepareViewResturant(Resturant resturant) {
        LOG.info("Inside prepareViewResturant with Resturant " + resturant.toString());
        this.resturant = resturant;
        return "/admin/viewResturant.xhtml";
    }

    /**
     * this method to fire action in the front end when clicking on Edit
     * Restaurant
     *
     * @param resturant
     * @return redirect the user to the edit Restaurant page of the admin pages
     */
    public String prepareEditResturant(Resturant resturant) {
        LOG.info("Inside prepareResturant with Resturant " + resturant.toString());
        this.resturant = resturant;
        return "/admin/editResturant.xhtml";
    }

    /**
     * this method to fire action in the front end when clicking on delete
     * Restaurant param and it does remove it from the db
     *
     * @param resturant
     * @return redirect the user to the welcome page of admin
     */
    public String doDeleteResturant(Resturant resturant) {
        LOG.info("Inside prepareDeleteResturant with Resturant " + resturant.toString());
        resturantSvc.remove(resturant);
        return "/admin/welcome.xhtml";
    }

    /**
     * this method creates a new restaurant in the db
     *
     * @return redirect the admin to the confirmation page of "restauarantok"
     */
    public String executeSaveRest() {
        LOG.info("inside ResturantController.executeSaveRest()" + resturant.toString());
        resturantSvc.create(resturant);
        return "/admin/resturantok.xhtml";
    }

    /**
     * this method is checking if this restaurant id exists in the db then,
     * update the restaurant information otherwise if there is no restaurant at
     * all, go and create a new restaurant in the db
     *
     * @return redirect the admin to welcome page
     */
    public String doSaveResturant() {
        // when complete, this method will invoke service layer
        // this method will need to be smart enough to know when to edit, and when to create
        LOG.info("Inside doSaveReservation with reservation " + resturant.toString());

        if (this.resturant.getId() != null) {
            LOG.info("Preparing to call an update in the service lyaer with " + this.resturant.toString());
            resturantSvc.editRestNoChangeToAdmin(resturant);
        } else {
            LOG.info("Preparing to create in the service lyaer with " + this.resturant.toString());
            resturantSvc.create(resturant);
        }
        return "/admin/welcome.xhtml?faces-redirect=true";
    }

    /**
     * Get the value of RestType[]
     *
     *
     * @return the value of RestType[]
     */
    public RestType[] getRestTypes() {
        return RestType.values();
    }

    /**
     * Get the list of the restaurants
     *
     *
     * @return all the restaurants that they are stored in the db
     */
    public List<Resturant> getAllRests() {
        return resturantSvc.findAll();
    }

    /**
     * Set the value of restaurant
     *
     * @param resturant new value of restaurant
     */
    public void setResturant(Resturant resturant) {
        this.resturant = resturant;
    }

    /**
     * Get the value of restaurant
     *
     * @return the value of restaurant
     */
    public Resturant getResturant() {
        return resturant;
    }

}
