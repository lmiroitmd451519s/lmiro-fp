/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.web;

import edu.iit.sat.itmd4515.lmiro.domain.AdminRest;
import edu.iit.sat.itmd4515.lmiro.domain.TableRest;
import edu.iit.sat.itmd4515.lmiro.service.AdminService;
import edu.iit.sat.itmd4515.lmiro.service.TabService;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author lynnmiro
 */
@Named
@RequestScoped
public class AdminRestController {

    private static final Logger LOG = Logger.getLogger(AdminRestController.class.getName());

    @EJB
    private AdminService adminSvc;
    @EJB
    private TabService tabSvc;

    @Inject
    LoginController loginController;

    private AdminRest adminRest;

    private TableRest tableRest;

    /**
     * this constructor of AdminRestController Class
     */
    public AdminRestController() {
    }

    @PostConstruct
    private void postConstruct() {
        LOG.info("AdminRestController is firing postConstruct()");
        adminRest = adminSvc.findByUsername(loginController.getRemoteUser());
        tableRest = new TableRest();
    }

    /**
     * this method to fire action in the front end when clicking on View Table
     *
     * @param tableRest
     * @return redirect the user to the view tableRest page of the Admin
     * Restaurant pages
     */
    public String prepareViewTableRest(TableRest tableRest) {
        LOG.info("Inside prepareViewTableRest with tableRest " + tableRest.toString());
        this.tableRest = tableRest;
        return "/adminrest/viewtable.xhtml";
    }

    /**
     * this method to fire action in the front end when clicking on Edit Table
     *
     * @param tableRest
     * @return redirect the user to the edit tableRest page of the Admin
     * Restaurant pages
     */
    public String prepareEditTableRest(TableRest tableRest) {
        LOG.info("Inside prepareEditTableRest with tableRest " + tableRest.toString());
        this.tableRest = tableRest;
        return "/adminrest/editTable.xhtml";
    }

    /**
     * this method creates a new table in the db
     *
     * @return redirect the user to the edit tableRest page of the Admin
     * Restaurant pages
     */
    public String prepareCreateTableRest() {
        LOG.info("Inside prepareEditTableRest with tableRest " + tableRest.toString());
        this.tableRest = new TableRest();
        return "/adminrest/viewtable.xhtml";
    }

    /**
     * this method to fire action in the front end when clicking on delete
     * tableRest param and it does remove it from the db
     *
     * @param tableRest
     * @return redirect the user to the welcome page of admin Restaurant
     */
    public String doDeleteTableRest(TableRest tableRest) {
        LOG.info("Inside doDeleteTableRest with tableRest " + tableRest.toString());
        tabSvc.remove(tableRest);
        return "adminrest/welcome.xhtml";
    }

    /**
     * this method is checking if this tableRest id exists in the db then,
     * update the table information otherwise if there is no restaurant at all,
     * go and create a new table in the db
     *
     * @return redirect the admin to welcome page
     */
    public String doSaveTable() {
        LOG.info("Inside doSaveTable with tableRest " + tableRest.toString());

        if (this.tableRest.getId() != null) {
            LOG.info("Preparing to call an update in the service lyaer with " + this.tableRest.toString());
            tabSvc.editResNoChangeToAdminRest(tableRest);
        } else {
            LOG.info("Preparing to create in the service lyaer with " + this.tableRest.toString());
            tabSvc.createAndAddAdminRests(tableRest, adminRest);
        }
        return "/adminrest/welcome.xhtml?faces-redirect=true";
    }

    /**
     * Get the value of adminRest
     *
     * @return the value of adminRest
     */
    public AdminRest getAdminRest() {
        return adminRest;
    }

    /**
     * Set the value of adminRest
     *
     * @param adminRest new value of adminRest
     */
    public void setAdminRest(AdminRest adminRest) {
        this.adminRest = adminRest;
    }

    /**
     * Get the value of tableRest
     *
     * @return the value of tableRest
     */
    public TableRest getTableRest() {
        return tableRest;
    }

    /**
     * Set the value of tableRest
     *
     * @param tableRest new value of tableRest
     */
    public void setTableRest(TableRest tableRest) {
        this.tableRest = tableRest;
    }

}
