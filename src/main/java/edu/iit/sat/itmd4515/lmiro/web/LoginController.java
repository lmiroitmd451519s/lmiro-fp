/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.web;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.security.enterprise.AuthenticationStatus;
import javax.security.enterprise.SecurityContext;
import javax.security.enterprise.authentication.mechanism.http.AuthenticationParameters;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.credential.Password;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;

/**
 *
 * @author lynnmiro
 */
@Named
@RequestScoped
public class LoginController {

    private static final Logger LOG = Logger.getLogger(LoginController.class.getName());

    @NotBlank(message = "You must enter a username")
    private String username;
    @NotBlank(message = "You must enter a password")
    private String password;

    @Inject
    private SecurityContext securityContext;
    @Inject
    private FacesContext facesContext;

    /**
     * constructor of LoginController class
     */
    public LoginController() {
    }

    /**
     * this method to give the user the appropriate context in the application
     *
     * @return the context of each user
     */
    public String getRemoteUser() {
        return facesContext.getExternalContext().getRemoteUser();
    }

    /**
     * this method is checking the roles of the user if admin or admin rest or
     * customer
     *
     * @return the security context of each user depends on the role
     */
    public String getRoles() {
        return securityContext.toString();
    }

    /**
     * this method is checking if this is a admin restaurant and return true or
     * false
     *
     * @return if the user is in the admin restaurant role by returning true or
     * false
     */
    public boolean isAdminRest() {
        return securityContext.isCallerInRole("ADMINREST_ROLE");
    }

    /**
     * this method is checking if this is a customer and return true or false
     *
     * @return if the user is in the customer role by returning true or false
     */
    public boolean isCustomer() {
        return securityContext.isCallerInRole("CUSTOMER_ROLE");
    }

    /**
     * this method is checking if this is an admin and return true or false
     *
     * @return if the user is in the admin role by returning true or false
     */
    public boolean isAdmin() {
        return securityContext.isCallerInRole("ADMIN_ROLE");
    }

    /**
     * this method is to fire the action of login in the front end. It starts by
     * checking the credentials of the username and password and authenticate
     * credentials if it is success login, the user will continue to welcome
     * page otherwise will go to error page
     *
     * @return redirect the user to the welcome page
     */
    public String doLogin() {
        LOG.info("inside doLogin method");
        Credential credential = new UsernamePasswordCredential(username, new Password(password));

        AuthenticationStatus status = securityContext.authenticate(
                (HttpServletRequest) facesContext.getExternalContext().getRequest(),
                (HttpServletResponse) facesContext.getExternalContext().getResponse(),
                AuthenticationParameters.withParams().credential(credential)
        );

        LOG.info("AuthenticationStatus is: " + status.toString());

        switch (status) {
            case SEND_CONTINUE:
                LOG.info("SEND_CONTINUE in login");
                break;
            case SEND_FAILURE:
                LOG.info("SEND_FAILURE in login");
                return "/error.xhtml";
            case SUCCESS:
                LOG.info("SUCCESS in login");
                break;
            case NOT_DONE:
                LOG.info("NOT_DONE in login");
                return "/error.xhtml";
        }

        return "/welcome.xhtml?faces-redirect=true";
    }

    /**
     * this method to logout the user of the application and if the request of
     * logout did not work then the user will be directed to the error page
     *
     * @return redirect the user to the sign in page
     */
    public String doLogout() {

        try {
            HttpServletRequest req = (HttpServletRequest) facesContext.getExternalContext().getRequest();
            req.logout();

        } catch (ServletException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return "/error.xhtml";
        }

        LOG.info("Logged out and redirecting to login page");
        return "/login.xhtml?faces-redirect=true";

    }

    /**
     * Get the value of username
     *
     * @return the value of username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set the value of username
     *
     * @param username new value of username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * set the value of password
     *
     * @return the value of password
     */
    public String getPassword() {
        return password;
    }

    /**
     * set the value of password
     *
     * @param password new value of password
     */
    public void setPassword(String password) {
        this.password = password;
    }

}
