/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.service;

import edu.iit.sat.itmd4515.lmiro.domain.Customer;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author lynnmiro
 */
@Named
@Stateless
public class CustService {
    @PersistenceContext(name = "itmd4515PU")
    private EntityManager em;
    
    /**
     *this CustService class Constructor
     */
    public CustService() {
    }
    
    /**
     *this method creates a new customer in the db
     * @param c customer
     */
    public void create (Customer c){
        em.persist(c);
    }
    
    /**
     *this method updates an existing customer in the db
     * @param c customer
     */
    public void update (Customer c){
        em.merge(c);
    }    
    
    /**
     *this method remove an existing customer in the db
     * @param c customer
     */
    public void remove (Customer c){
        em.remove(em.merge(c));
        
    }    
    
    
    /**
     * 
     * @param id The PK of an entity to find
     * @return return found entity or null
     */
    public Customer find (Long id){
        return em.find(Customer.class, id);
    }    
    
    /**
     * this method return the list of Customers in the db as
     * List<Customer>
     *
     * @return the list of Customers
     */
    public List<Customer> findAll(){
        return em.createNamedQuery("Customer.findAll", Customer.class).getResultList();
    } 

    /**
     *this method finds the customer by its username that defines in its POJO class
     * @param username
     * @return the customer found
     */
    public Customer findByUsername(String username){
        return em.createNamedQuery("Customer.findByUsername", Customer.class).setParameter("username", username).getSingleResult();
    }    
}
