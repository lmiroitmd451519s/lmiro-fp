/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.service;

import edu.iit.sat.itmd4515.lmiro.domain.security.Group;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author lynnmiro
 */
@Stateless
public class GroupService extends AbstractService<Group> {

    /**
     *this GroupService class constructor
     */
    public GroupService() {
        super(Group.class);
    }
    
    /**
     *this method return the list of group in the db
     * @return the list of groups
     * @return
     */
    @Override
    public List<Group> findAll() {
        return em.createNamedQuery("Group.findAll", entityClass).getResultList();
    }
    
}
