 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.domain;

/**
 *
 * @author lynnmiro
 */
public enum RestType {
    
    /**
     *my enum 
     */
    AMERICAN("American"),

    /**
     *my enum 
     */
    MEXICAN("Mexican"),

     /**
     *my enum 
     */
    ASIAN("Asian"),

    /**
     *my enum 
     */
    ITALIAN("Italian"),

      /**
     *my enum 
     */
    FRENCH("French");
    
    
    private String label;

    private RestType(String label) {
        this.label = label;
    }

    /**
     * Get the value of label
     * @return the value of label
     */
    public String getLabel() {
        return label;
    }
    
    
}
