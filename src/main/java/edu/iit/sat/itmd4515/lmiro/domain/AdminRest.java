/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.domain;

import edu.iit.sat.itmd4515.lmiro.domain.security.User;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author lynnmiro
 */
@Entity
@NamedQuery(name = "AdminRest.findAll", query = "select a from AdminRest a")
@NamedQuery(name = "AdminRest.findByName", query = "select a from AdminRest a where a.name = :name")
@NamedQuery(name = "AdminRest.findByUsername", query = "select a from AdminRest a where a.user.userName = :username")
public class AdminRest extends AbstractNamedEntity {

    //private String username;
    private String title;
    @OneToMany(mappedBy = "adminRest")
    private List<TableRest> tableRests = new ArrayList<>();
    @OneToOne
    @JoinColumn(name = "USERNAME")
    private User user;

    /**
     *AdminRest Class constructor
     */
    public AdminRest() {
    }

    /**
     *this AdminRest calls the super class and passes parameter name
     * @param name
     */
    public AdminRest(String name) {
        super(name);
    }

    public AdminRest(String title, User user) {
        this.title = title;
        this.user = user;
    }

    /**
     * Get the value of title
     * @return the value of title
     */
    public String getTitle() {
        return title;
    }

    /**
     * set the value of title
     * @param title new value of title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Get the value of user
     * @return the value of user
     */
    public User getUser() {
        return user;
    }

    /**
     *set the value of user
     * @param user new value of user
     */
    public void setUser(User user) {
        this.user = user;
    }
    /**
     *Get the value of List<TableRest>
     * @return the value of TableRest
     */
    public List<TableRest> getTableRests() {
        return tableRests;
    }

    /**
     *Set the value of tableRests
     * @param tableRests new value of tableRests List<TableRest>
     */
    public void setTableRests(List<TableRest> tableRests) {
        this.tableRests = tableRests;
    }

    /**
     * this method sets the tableRest of this admin restaurant and if there is no
     * tableRests made the tableRest will be added to the list of the
     * tableRest
     *
     * @param tableRest
     */
    public void addTableRest(TableRest tableRest) {

        tableRest.setAdminRest(this);

        if (!this.tableRests.contains(tableRest)) {
            this.tableRests.add(tableRest);
        }

    }
    @Override
    public String toString() {
        return "AdminRest{" + "title=" + title + ", user=" + user + '}';
    }
}
