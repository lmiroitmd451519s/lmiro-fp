/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.service;

import edu.iit.sat.itmd4515.lmiro.domain.AdminRest;
import edu.iit.sat.itmd4515.lmiro.domain.TableRest;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * @author lynnmiro
 */
@Named
@Stateless
public class TabService extends AbstractService<TableRest>{

    /**
     *this TabService constructor
     */
    public TabService() {
        super(TableRest.class);
    }

    /**
     *this method return the list of Tables in the db
     * @return the list of tables
     */
    @Override
    public List<TableRest> findAll() {
        return em.createNamedQuery("TableRest.findAll", entityClass).getResultList();
    }
    
    /**
     *
     * @param tr
     * @param a
     */
    public void createAndAddAdminRests(TableRest tr, AdminRest a){
        //; bring the front-end owner into persistence context
        a = em.getReference(AdminRest.class, a.getId());
        
        // create the new pet from user input
        super.create(tr);
        // making sure the new pet has its db generated id
        em.flush();
        
        // settting both sides of the relationship now
        a.addTableRest(tr);
        
        // merging the owner, because this entity is the owning side of the relationship
        // it controls the database updates and creates the FK
        em.merge(a);
    } 
   
    /**
     *
     * @param tabFromAdminRestForm
     */
    public void editResNoChangeToAdminRest(TableRest tabFromAdminRestForm) {

        // first I might get a reference to the pet currently in the database
        TableRest tabFromCDatabase = em.getReference(TableRest.class, tabFromAdminRestForm.getId());

        tabFromCDatabase.setCapacity(tabFromAdminRestForm.getCapacity());
        tabFromCDatabase.setReserved(tabFromAdminRestForm.isReserved());

        em.merge(tabFromCDatabase);
    }    
   
}
