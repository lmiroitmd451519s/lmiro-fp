/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.service;

import edu.iit.sat.itmd4515.lmiro.domain.security.User;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author lynnmiro
 */
@Stateless
public class UserService extends AbstractService<User> {

    /**
     *this UserService Constructor
     */
    public UserService() {
        super(User.class);
    }
    
    /**
     *this method return the list of users in the db
     * @return the list of users
     */
    @Override
    public List<User> findAll() {
        return em.createNamedQuery("User.findAll", entityClass).getResultList();
    }
    
}
