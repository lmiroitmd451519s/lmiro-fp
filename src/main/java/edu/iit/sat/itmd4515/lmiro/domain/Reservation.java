/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.domain;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 *
 * @author lynnmiro
 */
@Entity
@NamedQuery(name = "Reservation.findAll", query = "select v from Reservation v")
@NamedQuery(name = "Reservation.findById", query = "select v from Reservation v where v.id = :id")
public class Reservation extends AbstractIdentifiedEntity {

    //@FutureOrPresent(message = "Reservation can be made today or later")
    private LocalDate date;

    @NotBlank(message = "This field cannot be empty, please fill it out")
    private String time;
    @NotNull(message = "This field cannot be empty, please fill it out")
    private Integer partyNum;
    //owning side of the relationship
    //owning side gets the FK in the db and controls the db updates
    @ManyToOne
    private Customer customer;
    @ManyToOne
    private Resturant resturant;
    @OneToOne
    private TableRest tableRest;

    /**
     *
     */
    public Reservation() {
    }

    /**
     *
     * @param date
     * @param time
     * @param partyNum
     */
    public Reservation(LocalDate date, String time, Integer partyNum) {
        this.date = date;
        this.time = time;
        this.partyNum = partyNum;
    }

    /**
     * Get the value of date
     *
     * @return the value of date
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * Set the value of date
     *
     * @param date new value of date
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * Get the value of time
     * @return the value of time
     */
    public String getTime() {
        return time;
    }

    /**
     *set the value of time
     * @param time new value of time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     *get the value of partyNum
     * @return the value of partyNum
     */
    public Integer getPartyNum() {
        return partyNum;
    }

    /**
     *set the value of partyNum
     * @param partyNum new value of partyNum
     */
    public void setPartyNum(Integer partyNum) {
        this.partyNum = partyNum;
    }

    /**
     *get the value of customer
     * @return the value of customer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     *set the value of customer
     * @param customer new value of customer
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**
     *get the value of resturant
     * @return the value of restauant 
     */
    public Resturant getResturant() {
        return resturant;
    }

    /**
     *set the value of Resturant
     * @param resturant new value of Resturant
     */
    public void setResturant(Resturant resturant) {
        this.resturant = resturant;
    }

    /**
     *get the value of tableRest
     * @return the value of tableRest
     */
    public TableRest getTableRest() {
        return tableRest;
    }

    /**
     *set the value of tableRest
     * @param tableRest new value of tableRest
     */
    public void setTableRest(TableRest tableRest) {
        this.tableRest = tableRest;
        getTableRest().setReservation(this);

    }

    @Override
    public String toString() {
        return "Reservation{" + "date=" + date + ", time=" + time + ", partyNum=" + partyNum + ", customer=" + customer + ", resturant=" + resturant + ", tableRest=" + tableRest + '}';
    }

}
