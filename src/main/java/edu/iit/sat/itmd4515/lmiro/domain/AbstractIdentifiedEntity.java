/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.domain;

import java.time.LocalDateTime;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;

/**
 *
 * @author lynnmiro
 */
@MappedSuperclass
public abstract class AbstractIdentifiedEntity {

    /**
     *generate id property as long data type
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Version
    private Long version;
    private LocalDateTime lastUpdatedTimeStamp;
    private LocalDateTime createdTimeStamp;

    @PrePersist
    private void prePersist() {
        createdTimeStamp = LocalDateTime.now();
        lastUpdatedTimeStamp = LocalDateTime.now();
    }

    @PreUpdate
    private void preUpdate() {
        lastUpdatedTimeStamp = LocalDateTime.now();
    }

    /**
     *AbstractIdentifiedEntity Class constructor
     */
    public AbstractIdentifiedEntity() {
    }

    /**
     *get the value of id
     * @return the value of id
     */
    public Long getId() {
        return id;
    }

    /**
     *set the value of id
     * @param id new value of id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *get the value of version
     * @return the value of version
     */
    public Long getVersion() {
        return version;
    }

    /**
     *set the value of version
     * @param version new value of version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

}
