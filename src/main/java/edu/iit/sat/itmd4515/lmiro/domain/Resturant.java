/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 *
 * @author lynnmiro
 */
@Entity
@NamedQuery(name = "Resturant.findAll", query = "select r from Resturant r")
@NamedQuery(name = "Resturant.findByName", query = "select r from Resturant r where r.name = :name")
public class Resturant extends AbstractNamedEntity {

    // inverse side of the relationship
    @OneToMany(mappedBy = "resturant")
    private List<Reservation> reservations = new ArrayList<>();

    // inverse side of the relationship
    @OneToMany(mappedBy = "resturant")
    private List<TableRest> tables = new ArrayList<>();
    @Enumerated(EnumType.STRING)
    private RestType type;
    @NotNull
    @Column(nullable = false)
    private Integer cost;

    /**
     * this Resturant Class constructor
     */
    public Resturant() {
    }

    /**
     *
     * @param name
     * @param type
     * @param cost
     */
    public Resturant(String name, RestType type, Integer cost, LocalDate date) {
        this.name = name;
        this.type = type;
        this.cost = cost;
    }

    /**
     * Get the value of type
     *
     * @return the value of type
     */
    public RestType getType() {
        return type;
    }

    /**
     * set the value of type
     *
     * @param type new value of type
     */
    public void setType(RestType type) {
        this.type = type;
    }

    /**
     * Get the value of cost
     *
     * @return the value of cost
     */
    public Integer getCost() {
        return cost;
    }

    /**
     * set the value of cost
     *
     * @param cost new value of cost
     */
    public void setCost(Integer cost) {
        this.cost = cost;
    }

    /**
     * Get the value of reservations
     *
     * @return the value of reservations
     */
    public List<Reservation> getReservations() {
        return reservations;
    }

    /**
     * set the value of reservations
     *
     * @param reservations new value of reservations
     */
    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    /**
     * Get the value of tables
     *
     * @return the value of tables
     */
    public List<TableRest> getTables() {
        return tables;
    }

    /**
     * set the value of tables
     *
     * @param tables new value of tables
     */
    public void setTables(List<TableRest> tables) {
        this.tables = tables;
    }

    @Override
    public String toString() {
        return "Resturant{" + "id=" + id + ", name=" + name + ", type=" + type + ", cost=" + cost + '}';
    }

}
