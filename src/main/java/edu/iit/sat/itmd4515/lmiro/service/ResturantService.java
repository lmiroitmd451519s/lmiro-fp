/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.service;

import edu.iit.sat.itmd4515.lmiro.domain.Customer;
import edu.iit.sat.itmd4515.lmiro.domain.Reservation;
import edu.iit.sat.itmd4515.lmiro.domain.Resturant;
import edu.iit.sat.itmd4515.lmiro.domain.security.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * @author lynnmiro
 */
@Named
@Stateless
public class ResturantService extends AbstractService<Resturant> {

    /**
     * this ResturantService Class constructor
     */
    public ResturantService() {
        super(Resturant.class);

    }

    /**
     * this method return the list of resturants in the db as List<Resturant>
     *
     * @return the list of resturants
     */
    @Override
    public List<Resturant> findAll() {
        return em.createNamedQuery("Resturant.findAll", entityClass).getResultList();

    }

    /**
     * This service method accept a Restaurant POJO representing the input from
     * the admin form. This method will only update the name, type, and cost of
     * the Restaurant. It will not change the admin.
     *
     * @param restFromAdminForm
     */
    public void editRestNoChangeToAdmin(Resturant restFromAdminForm) {
        Resturant restFromCDatabase = em.getReference(Resturant.class, restFromAdminForm.getId());

        restFromCDatabase.setName(restFromAdminForm.getName());
        restFromCDatabase.setType(restFromAdminForm.getType());
        restFromCDatabase.setCost(restFromAdminForm.getCost());

        em.merge(restFromCDatabase);
    }

}
