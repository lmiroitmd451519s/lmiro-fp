/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.service;

import edu.iit.sat.itmd4515.lmiro.domain.AdminRest;
import edu.iit.sat.itmd4515.lmiro.domain.Customer;
import edu.iit.sat.itmd4515.lmiro.domain.Reservation;
import edu.iit.sat.itmd4515.lmiro.domain.RestType;
import edu.iit.sat.itmd4515.lmiro.domain.Resturant;
import edu.iit.sat.itmd4515.lmiro.domain.TableRest;
import edu.iit.sat.itmd4515.lmiro.domain.security.Group;
import edu.iit.sat.itmd4515.lmiro.domain.security.User;
import java.time.LocalDate;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author lynnmiro
 */
@Startup
@Singleton
public class StartupSeedDatabase {

    private static final Logger LOG = Logger.getLogger(StartupSeedDatabase.class.getName());
    @PersistenceContext(name = "itmd4515PU")
    private EntityManager em;

    @EJB
    private CustService custSvc;
    @EJB
    private AdminService adminSvc;
    @EJB
    private ResService resSvc;
    @EJB
    private ResturantService resturantSvc;
    @EJB
    private TabService tabSvc;
    @EJB
    private UserService userSvc;
    @EJB
    private GroupService groupSvc;

    /**
     * StartupSeedDatabase Class Constructor
     */
    public StartupSeedDatabase() {
    }

    @PostConstruct
    private void seedDatabase() {
        // create admin user and group
        User adminUser = new User("admin", "admin", true);
        Group adminGroup = new Group("ADMIN_GROUP", "This is the identity store group for administrators.");
        adminUser.addGroup(adminGroup);
        groupSvc.create(adminGroup);
        userSvc.create(adminUser);

        //business users and groups
        Group custGroup = new Group("CUSTOMER_GROUP", "identity store group of customers");
        Group adminRestGroup = new Group("ADMINREST_GROUP", "identity store group of adminrests");

        User customer1 = new User("customer1", "customer1", true);
        customer1.addGroup(custGroup);
        User customer2 = new User("customer2", "customer2", true);
        customer2.addGroup(custGroup);
        User adminRest1 = new User("adminRest1", "adminRest1", true);
        adminRest1.addGroup(adminRestGroup);
        User adminRest2 = new User("adminRest2", "adminRest2", true);
        adminRest2.addGroup(adminRestGroup);
        adminRest2.addGroup(custGroup);

        groupSvc.create(custGroup);
        groupSvc.create(adminRestGroup);
        userSvc.create(customer1);
        userSvc.create(customer2);
        userSvc.create(adminRest1);
        userSvc.create(adminRest2);

        //asscoiate 
        Customer c1 = new Customer("Customer One");
        c1.setUser(customer1);
        Customer c2 = new Customer("Customer Two");
        c2.setUser(customer2);
        AdminRest a1 = new AdminRest("AdminRest One");
        a1.setUser(adminRest1);
        AdminRest a2 = new AdminRest("AdminRest Two");
        a2.setUser(adminRest2);
        Customer c3 = new Customer("AdminRest Two Customer");
        c3.setUser(adminRest2);

        Resturant r1 = new Resturant("woa", RestType.ASIAN, 2, LocalDate.of(2019, 07, 20));
        Resturant r2 = new Resturant("pasta bowl", RestType.ITALIAN, 2, LocalDate.of(2019, 06, 20));

        Reservation v1 = new Reservation(LocalDate.of(2019, 4, 14), "12:00", 4);

        Reservation v2 = new Reservation(LocalDate.of(2019, 3, 12), "12:00", 1);
        Reservation v3 = new Reservation(LocalDate.of(2019, 4, 12), "12:00", 7);

        TableRest tr1 = new TableRest(5, true);
        TableRest tr2 = new TableRest(3, true);
        v1.setTableRest(tr1);

        resSvc.create(v1);
        tabSvc.create(tr1);
        tabSvc.create(tr2);

        resSvc.create(v2);
        resSvc.create(v3);

        custSvc.create(c3);
        custSvc.create(c2);
        custSvc.create(c1);
        custSvc.update(c2);

        adminSvc.create(a1);
        adminSvc.create(a2);

        resturantSvc.create(r1);
        resturantSvc.create(r2);
        c1.addReservation(v1);
        c3.addReservation(v2);
        c2.addReservation(v3);

        a1.addTableRest(tr1);
        a2.addTableRest(tr2);
    }
}
