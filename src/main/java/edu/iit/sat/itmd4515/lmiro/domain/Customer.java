/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.domain;

import edu.iit.sat.itmd4515.lmiro.domain.security.User;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author lynnmiro
 */
@Entity
@NamedQuery(name = "Customer.findAll", query = "select c from Customer c")
@NamedQuery(name = "Customer.findByName", query = "select c from Customer c where c.name = :name")
@NamedQuery(name = "Customer.findByUsername", query = "select c from Customer c where c.user.userName = :username")
public class Customer extends AbstractNamedEntity {

    private String lastName;
    private String email;
    private String phone;

    @OneToMany(mappedBy = "customer")
    private List<Reservation> reservations = new ArrayList<>();
    @OneToOne
    @JoinColumn(name = "USERNAME")
    private User user;

    /**
     * Customer Class constructor
     */
    public Customer() {
    }

    /**
     * Customer Class constructor and it is calling the super class
     *
     * @param name new value of name
     */
    public Customer(String name) {
        super(name);
    }

    /**
     *
     * @param lastName
     * @param email
     * @param phone
     */
    public Customer(String lastName, String email, String phone) {
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
    }

    /**
     * Get the value of lastName
     *
     * @return the value of lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * set the values of lastName
     *
     * @param lastName new value of lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Get the value of email
     *
     * @return the value of email
     */
    public String getEmail() {
        return email;
    }

    /**
     * set the value of email
     *
     * @param email new value of email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get the value of phone
     *
     * @return the value of phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * set the value of phone
     *
     * @param phone new value of phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * this method sets the reservation of this customer and if there is no
     * reservations made the reservation will be added to the list of the
     * reservations
     *
     * @param reservation
     */
    public void addReservation(Reservation reservation) {

        reservation.setCustomer(this);

        if (!this.reservations.contains(reservation)) {
            this.reservations.add(reservation);
        }
    }

    /**
     * this method takes reservation and if this reservation is in the list of
     * reservations, then this reservation will be deleted
     *
     * @param reservation from POJO Reservation
     */
    public void removeReservation(Reservation reservation) {
        if (this.reservations.contains(reservation)) {
            this.reservations.remove(reservation);
        }
        reservations.remove(reservation.getCustomer());
    }

    /**
     * Get the value of reservations
     *
     * @return the value of reservations
     */
    public List<Reservation> getReservations() {
        return reservations;
    }

    /**
     * set the value of reservations
     *
     * @param reservations new value of reservations
     */
    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    /**
     * Get the value of user
     *
     * @return the value of user
     */
    public User getUser() {
        return user;
    }

    /**
     * set the value of user
     *
     * @param user new value of user
     */
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Customer{" + "lastName=" + lastName + ", email=" + email + ", phone=" + phone + '}';
    }
}
