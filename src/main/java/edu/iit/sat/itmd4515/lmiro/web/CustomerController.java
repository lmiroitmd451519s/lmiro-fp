/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.web;

import edu.iit.sat.itmd4515.lmiro.domain.Customer;
import edu.iit.sat.itmd4515.lmiro.domain.Reservation;
import edu.iit.sat.itmd4515.lmiro.service.CustService;
import edu.iit.sat.itmd4515.lmiro.service.ResService;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author lynnmiro
 */
@Named
@RequestScoped
public class CustomerController {

    private static final Logger LOG = Logger.getLogger(CustomerController.class.getName());

    @EJB
    private CustService custSvc; 
    @EJB
    private ResService resSvc;
    
    @Inject
    LoginController loginController;
    
    private Customer customer;

    private Reservation reservation;

    /**
     * constructor of CustomerController Class
     */
    public CustomerController() {
    }


    @PostConstruct
    private void postConstruct() {
        LOG.info("CustomerController is firing postConstruct()");
        customer = custSvc.findByUsername(loginController.getRemoteUser());
        reservation = new Reservation();
    } 
    
    /**
     * this method to fire action in the front end when clicking on View
     * Reservation
     *
     * @param reservation
     * @return redirect the user to the view Reservation page of the customer pages
     */
    public String prepareViewReservation(Reservation reservation) {
        LOG.info("Inside prepareViewReservation with reservation " + reservation.toString());
        this.reservation = reservation;
        return "/customer/viewReservation.xhtml";
    }  
    
   /**
     * this method to fire action in the front end when clicking on Edit
     * Reservation
     *
     * @param reservation
     * @return redirect the user to the edit Reservation page of the customer pages
     */
    public String prepareEditReservation(Reservation reservation) {
        LOG.info("Inside prepareEditReservation with reservation " + reservation.toString());
        this.reservation = reservation;
        return "/customer/editReservation.xhtml";
    }  
    
     /**
     * this method creates a new reservation in the db
     *
     * @return redirect the user to the edit Reservation page of the customer pages
     */
    public String prepareCreateReservation() {
        LOG.info("Inside prepareCreateReservation with reservation " + reservation.toString());
        this.reservation = new Reservation();
        return "/customer/editReservation.xhtml";
    } 
   
    /**
     * this method to fire action in the front end when clicking on delete
     * reservation param and it does remove it from the db
     *
     * @param reservation
     * @return redirect the user to the welcome page of customer
     */
    public String doDeleteReservation(Reservation reservation) {
        LOG.info("Inside doDeleteReservation with reservation " + reservation.toString());
        resSvc.remove(reservation);  
        return "/customer/welcome.xhtml";
    }    
    

    /**
     * this method is checking if this reservation id exists in the db then,
     * update the reservation information otherwise if there is no reservation at
     * all, go and create a new reservation in the db
     *
     * @return redirect the customer to welcome page
     */
    public String doSaveReservation() {
        LOG.info("Inside doSaveReservation with reservation " + reservation.toString());
        
        if(this.reservation.getId() != null){
            LOG.info("Preparing to call an update in the service lyaer with " + this.reservation.toString());
            resSvc.editResNoChangeToCustomer(reservation);
        } else {
            LOG.info("Preparing to create in the service layer with " + this.reservation.toString());
            resSvc.createWithCustomer(reservation, customer);
        }
        return "/customer/welcome.xhtml?faces-redirect=true";
    }    


    
   /**
     * Get the value of customer
     *
     * @return the value of customer
     */
    public Customer getCustomer() {
        return customer;
    }

     /**
     * Set the value of customer
     *
     * @param customer new value of customer
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

     /**
     * Get the value of reservation
     *
     * @return the value of reservation
     */
    public Reservation getReservation() {
        return reservation;
    }

     /**
     * Set the value of reservation
     *
     * @param reservation new value of reservation
     */
    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    
}
