/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

/**
 *
 * @author lynnmiro
 */
@Entity
@NamedQuery(name = "TableRest.findAll", query = "select tr from TableRest tr")
@NamedQuery(name = "TableRest.findById", query = "select tr from TableRest tr where tr.id = :id")
public class TableRest extends AbstractIdentifiedEntity {

    //owning side of the relationship    
    @ManyToOne
    private Resturant resturant;
    //inverse side
    @OneToOne(mappedBy = "tableRest")
    private Reservation reservation;
    //owning side of the relationship
    //owning side gets the FK in the db and controls the db updates
    //@ManyToOne 
    @ManyToOne
    private AdminRest adminRest;

    @NotNull(message = "Capacity cannot be empty, please fill it out")
    private Integer capacity;

    private boolean reserved;

    /**
     * this TableRest Class constructor
     */
    public TableRest() {
    }

    /**
     *
     * @param capacity
     * @param reserved
     */
    public TableRest(Integer capacity, boolean reserved) {
        this.capacity = capacity;
        this.reserved = reserved;
    }

    /**
     *
     * @param reservation
     * @param capacity
     * @param reserved
     */
    public TableRest(Reservation reservation, Integer capacity, boolean reserved) {
        // this.reservation = reservation;
        this.capacity = capacity;
        this.reserved = reserved;
    }

    /**
     * Get the value of capacity
     *
     * @return the value of capacity
     */
    public Integer getCapacity() {
        return capacity;
    }

    /**
     * Set the value of capacity
     *
     * @param capacity new value of capacity
     */
    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    /**
     * get the value of reserved
     *
     * @return the value of reserved
     */
    public boolean isReserved() {
        return reserved;
    }

    /**
     * set the value of reserved as boolean data type
     *
     * @param reserved new value of reserved
     */
    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }

    /**
     * get the value of restaurant
     *
     * @return the value of restaurant
     */
    public Resturant getResturant() {
        return resturant;
    }

    /**
     * set the value of restaurant of POJO Restaurant
     *
     * @param resturant new value restaurant
     */
    public void setResturant(Resturant resturant) {
        this.resturant = resturant;
    }

    /**
     * get the value of Reservation
     *
     * @return the value of reservation
     */
    public Reservation getReservation() {
        return reservation;
    }

    /**
     * set the value of reservation of POJO Reservation
     *
     * @param reservation new value reservation
     */
    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    /**
     * get the value of AdminRest
     *
     * @return the value of adminRest
     */
    public AdminRest getAdminRest() {
        return adminRest;
    }

    /**
     * set the value of adminRest of POJO AdminRest
     *
     * @param adminRest new value adminRest
     */
    public void setAdminRest(AdminRest adminRest) {
        this.adminRest = adminRest;
    }

    @Override
    public String toString() {
        return "TableRest{" + "capacity=" + capacity + ", reserved=" + reserved + '}';
    }

}
