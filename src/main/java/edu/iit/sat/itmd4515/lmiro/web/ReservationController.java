/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.web;

import edu.iit.sat.itmd4515.lmiro.domain.Reservation;
import edu.iit.sat.itmd4515.lmiro.service.ResService;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author lynnmiro
 */
@Named
@RequestScoped
public class ReservationController {

    private static final Logger LOG = Logger.getLogger(ReservationController.class.getName());

    @EJB
    private ResService resSvc;

    private Reservation reservation;

    @Inject
    LoginController loginController;

    /**
     * this constructor of ReservationController Class
     */
    public ReservationController() {
    }

    @PostConstruct
    private void postConstruct() {
        LOG.info("inside restController.postConstruct()");
        reservation = new Reservation();
    }

    /**
     * get the value of reservation
     *
     * @return the value of reservation
     */
    public Reservation getReservation() {
        return reservation;
    }

    /**
     * Set the value of reservation
     *
     * @param reservation new value of reservation
     */
    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

}
