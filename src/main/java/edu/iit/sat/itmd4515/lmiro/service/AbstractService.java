/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author lynnmiro
 * @param <T>
 */

//class that is abstract and have a generic datatype
public abstract class AbstractService<T> {

    /**
     *defining our properties and getting the entity manager
     */
    @PersistenceContext(name = "itmd4515PU")
    protected EntityManager em;

    /**
     *property entityClass of generic class <T>
     */
    protected Class<T> entityClass;
    
    /**
     * Set the value of AbstractService
     * @param entityClass
     */
    protected AbstractService(Class entityClass) {
        this.entityClass = entityClass;
    }
      
    /**
     *this method creates a new generic entity in the db
     * @param entity from T 
     */
    public void create (T entity){
        em.persist(entity);
    }
    
    /**
     *this method updates an existing generic entity in the db
     * @param entity
     */
    public void update (T entity){
        em.merge(entity);
    }    
    
    /**
     *this method removes an existing generic entity in the db
     * @param entity
     */
    public void remove (T entity){
        em.remove(em.merge(entity));
        
    } 
    
  
    /**
     * 
     * @param id The PK of an entity to find
     * @return return found entity or null
     */
    public T find (Object id){
        return em.find(entityClass, id);
    }    
    
    /**
     *needs to be overridden to return a list of generic data type
     * @return
     */
    public abstract List<T> findAll();   
    
    
}
