/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.logging.Logger;
import javax.persistence.RollbackException;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author lynnmiro
 */
public class ResturantJPATest extends AbstractJPATest{
    
    private static final Logger LOG = Logger.getLogger(ResturantJPATest.class.getName());
    
    /**
     *
     */
    @Test
    
    public void testFindExisitingResturant (){
        
        Resturant wild = em.createNamedQuery("Resturant.findByName", Resturant.class)
                .setParameter("name", "WILD")
                .getSingleResult();
        LOG.info(wild.toString());        
        assertTrue("Name should match", "WILD".equals(wild.getName()));
        
        
    }
    
   // test a removal by creating a new pet, and then removing
    // assert that you can no longer find it from the database

    /**
     *
     */
    
    @Test
    public void testRemoveResturant(){
        Resturant dry = new Resturant("Dry", RestType.AMERICAN, 1, LocalDate.of(2019, 7, 20));
        tx.begin();
        em.persist(dry);
        em.remove(dry); 
        tx.commit(); 
   
        assertNull("ID should be null", dry.getId());
       
    }
    
    // test an update to the seed data
    // first, find the seed data and assert it is what you expect (from @Before fixture)
    // then make an update and commit
    // remember, an update can be em.merge or you can call set methods on a managed entity, inside the transaction
    // read it back from the database
    // assert the update is what you expect

    /**
     *
     */
    @Test
    public void testUpdateResturant(){
        
        Resturant wild = em.createNamedQuery("Resturant.findByName", Resturant.class)
                .setParameter("name", "WILD")
                .getSingleResult();
        assertTrue("Name should match", "WILD".equals(wild.getName()));
        
        tx.begin();
        em.persist(wild);
        wild.setType(RestType.AMERICAN);
        tx.commit();
        //assertTrue("Type should match", "Dry".equals(wild.getType()));
        
    }    

    /**
     *
     */
    @Test
    public void testCreateNewResturant (){
        Resturant t = new Resturant("valid escencia2",RestType.MEXICAN, 3, LocalDate.of(2019, 3, 1));
        tx.begin();
        assertNull("ID should be null", t.getId());
        em.persist(t);
        assertNull("ID should be null", t.getId());        
        tx.commit();
        assertNotNull("ID should not be null", t.getId());
        LOG.info(t.toString());
        assertTrue("Id should be greater than 0", t.getId() > 0l);
        
        tx.begin();
        em.remove(t);
        tx.commit();
        
    } 
//    

    /**
     *
     */
    @Test(expected = RollbackException.class)
    public void testCreateNewinValidResturant (){
        Resturant t = new Resturant(null, RestType.FRENCH, 2, LocalDate.of(2019, 3, 1));
        tx.begin();
        assertNull("ID should be null", t.getId());
        em.persist(t);
        assertNull("ID should be null", t.getId());        
        tx.commit();
        assertNotNull("ID should not be null", t.getId());
        LOG.info(t.toString());
        assertTrue("Id should be greater than 0", t.getId() > 0l);
        
    }
    
    /**
     *
     */
    @Test
    public void testResturantReservationManytoOneBiDirectionalRelationship (){
        //Resturant t = new Resturant("escencia", "mexican", 3, LocalDate.of(2019, 3, 1));
    
//        //owning side
        Reservation r = new Reservation(LocalDate.of(2019, 4, 1), "2:00", 2);
//        //inverse side
        TableRest tr = new TableRest(3, true);
        
        r.setTableRest(tr);
       
        


        tx.begin();
        em.persist(tr);
        em.persist(r);
        tx.commit();
        
//       //find the owning side 
        Reservation findReservation = em.find(Reservation.class, 1l);
        System.out.println("Reservation date is: " + findReservation.getDate());
        assertEquals(r.getDate(), findReservation.getDate());
        System.out.println("Table of the reservation: " + findReservation.getTableRest().toString());
        assertEquals(r.getTableRest().getCapacity(), findReservation.getTableRest().getCapacity());
        
        
//        
//        //find the inverse side
        TableRest findTableRest = em.find(TableRest.class, 1l);
        System.out.println("Table capacity: " + findTableRest.getCapacity());
        assertEquals(tr.getCapacity(), findTableRest.getCapacity());
        System.out.println("Table of the reservation: " + findTableRest.getReservation().toString());
        assertEquals(tr.getReservation().getDate(), findTableRest.getReservation().getDate());
     
        
        tx.begin();
        em.remove(r);
        em.remove(tr); 
        tx.commit();
//        
//        findReservation = em.find(Reservation.class, 1l);
//        assertTrue("Reservation tables should be 0", findReservation.getId() == 0);
//        
        
    }
}
