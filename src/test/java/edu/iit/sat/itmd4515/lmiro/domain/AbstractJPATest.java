/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.lmiro.domain;

import java.time.LocalDate;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author lynnmiro
 */
public class AbstractJPATest {
    private static EntityManagerFactory emf;

    /**
     *
     */
    protected EntityManager em;

    /**
     *
     */
    protected EntityTransaction tx;
    
    /**
     *
     */
    @BeforeClass
    public static void beforeClassTestFixtureRunsOncePerClass(){
        emf = Persistence.createEntityManagerFactory("itmd4515testPU");
        
    }

    /**
     *
     */
    @AfterClass
    public static void afterClassTestFixtureRunsOncePerClass(){
        
        if (emf != null){
            emf.close();
        }
        
    }

    /**
     *
     */
    @Before
    public void beforeEachTestFixture(){
        em = emf.createEntityManager();
        tx = em.getTransaction();
        
        Resturant wild = new Resturant("WILD", RestType.AMERICAN, 1, LocalDate.of(2019, 7, 20));
        tx.begin();
        em.persist(wild);
        tx.commit();
                
    }     

    /**
     *
     */
    @After
    public void afterEachTestFixture(){
        Resturant wild = em.createNamedQuery("Resturant.findByName", Resturant.class)
                .setParameter("name", "WILD")
                .getSingleResult(); 
        tx.begin();
        em.remove(wild);
        tx.commit();
        if (em != null){
            em.close();           
        }
        
    }    
    
    
}
